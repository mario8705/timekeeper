function ready(fn) {
    if (document.readyState === 'complete') {
        fn();
    } else {
        window.addEventListener('DOMContentLoaded', fn, false);
    }
}

function zeroPad(str, n) {
    if (typeof str !== 'string') {
        str = str + '';
    }

    if (typeof String.prototype.padStart !== 'undefined') {
        return str.padStart(n, '0');
    } else {
        while (str.length < n) {
            str = '0' + str;
        }

        return str;
    }
}

function getSecondsOfDay() {
    var currentDate = new Date();
    var hours = currentDate.getHours();
    var minutes = currentDate.getMinutes();
    var seconds = currentDate.getSeconds();

    var daySeconds = (hours * 60 + minutes) * 60 + seconds;
    return daySeconds;
}

function isPauseActive(pause) {
    var daySeconds = getSecondsOfDay();
    return (daySeconds >= pause.start && daySeconds < pause.start + pause.end);
}

function calcDiffPause(pause) {
    var daySeconds = getSecondsOfDay();

    return pause.start - daySeconds;
}

function getNearestPause(currentDate) {
    if (typeof currentDate === 'undefined') {
        currentDate = new Date();
    }

    var daySeconds = getSecondsOfDay();

    var pause, nearestPause = null, i;
    for (i = 0; i < pauses.length; ++i) {
        pause = pauses[i];

        if (isPauseActive(pause)) {
            return pause;
        }

        if (pause.start + pause.end > daySeconds) {
            if (nearestPause === null) {
                nearestPause = pause;
                continue;
            }

            if (nearestPause.start > pause.start) {
                nearestPause = pause;
            }
        }
    }

    return nearestPause;
}

function formatTime(seconds) {
    var minutes = Math.floor(seconds / 60);
    var hours = Math.floor(minutes / 60);
    seconds = seconds % 60;

    if (hours === 0) {
        if (minutes === 0) {
            return `${seconds} secs`;
        } else if (hours === 0) {
            return `${zeroPad(minutes, 2)}:${zeroPad(seconds % 60, 2)}`;
        }
    } else {
        return `${hours}:${zeroPad(minutes, 2)}:${zeroPad(seconds % 60, 2)}`;
    }
}

var pauses = [];

function addPause(name, startHour, startMinute, durationMinutes) {
    pauses.push({
        name: name,
        start: (startHour * 60 + startMinute) * 60,
        end: durationMinutes * 60,
    });
}

addPause('Pause du matin 1', 9, 45, 15);
addPause('Pause du matin 1', 11, 0, 15);
addPause('Pause du midi', 12, 30, 70);
addPause('Pause de l\'après midi 1', 15, 0, 15);
addPause('Pause de l\'après midi 2', 16, 30, 15);

ready(function() {
    var greenElement = document.getElementsByClassName('green')[0];
    var redElement = document.getElementsByClassName('red')[0];
    var timeElement = document.getElementById('time');

    function setVisible(el, state) {
        el.style.display = state ? 'initial' : 'none';
    }

    function toggleElements(red) {
        setVisible(greenElement, !red);
        setVisible(redElement, red);
    }

    function updateDisplay() {
        var nearestPause = getNearestPause();

        if (nearestPause === null) {
            toggleElements(true);
            timeElement.innerText = "NOPE";
        } else if (isPauseActive(nearestPause)) {
            toggleElements(false);
        } else {
            toggleElements(true);
            timeElement.innerText = formatTime(calcDiffPause(nearestPause));
        }
    }

    updateDisplay();

    setInterval(function() {
        updateDisplay();
    }, 500);
});